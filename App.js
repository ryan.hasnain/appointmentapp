/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import {Home} from './src/features/Home';
import {store} from './src/redux/store';
import { BookSlotScreen, SlotSelectionScreen } from './src/features/BookAppointment';
import { BOOK_SLOT_SCREEN, HOME_SCREEN, SLOT_SELECTION_SCREEN } from './src/constants/appointment.constant';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name= {HOME_SCREEN}
            component={Home}
            options={{title: 'Home'}}
          />
          <Stack.Screen
            name={SLOT_SELECTION_SCREEN}
            component={SlotSelectionScreen}
            options={{title: 'Select Available Slot'}}
          />
           <Stack.Screen
            name={BOOK_SLOT_SCREEN}
            component={BookSlotScreen}
            options={{title: 'Book Slot'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
