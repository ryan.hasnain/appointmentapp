import moment from 'moment';

export const getTodayDate = () => {
  return moment().format('MM/DD/YYYY');
};

export const getMinDate = () => {
  return moment().add(1, 'days').format('MM/DD/YYYY');
};

export const getMaxDate = () => {
  return moment().add(2, 'days').format('MM/DD/YYYY');
};

export const getSelectedDate = date => {
  return moment(new Date(date)).format('MM/DD/YYYY');
};
