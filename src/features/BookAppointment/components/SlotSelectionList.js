import React, {useState, useEffect} from 'react';
import {Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

function SlotSelectionList(props) {
  const [selectedId, setSelectedId] = useState(null);

  useEffect(() => {
    setSelectedId(null);
  }, [props.selectedDate]);

  const Item = ({item, onPress}) => {
    const {title, isAvailable, id} = item;

    const slotStyle =
      id === selectedId ? styles.selectedStyle : styles.defaultStyle;
    const textStyle = isAvailable
      ? styles.availableTextStyle
      : styles.bookedTextStyle;
    return (
      <TouchableOpacity
        style={slotStyle}
        onPress={onPress}
        disabled={isAvailable ? false : true}>
        <Text style={textStyle}>{title}</Text>
        <Text style={textStyle}>{isAvailable ? 'Available' : 'Booked'}</Text>
      </TouchableOpacity>
    );
  };

  const onPress = item => {
    props.onSlotSelect(item);
    setSelectedId(item.id);
  };

  const renderItem = ({item}) => {
    return <Item item={item} onPress={() => onPress(item)} />;
  };

  return (
    <FlatList
      style={styles.listStyle}
      data={props.slotByDateList}
      renderItem={renderItem}
      extraData={selectedId}
      keyExtractor={item => item.id}
    />
  );
}

const styles = StyleSheet.create({
  selectedStyle: {
    backgroundColor: '#696969',
    padding: 10,
    marginVertical: 2,
    marginHorizontal: 0,
  },
  defaultStyle: {
    backgroundColor: '#DCDCDC',
    padding: 10,
    marginVertical: 2,
    marginHorizontal: 0,
  },
  availableTextStyle: {
    fontSize: 15,
  },
  bookedTextStyle: {
    fontSize: 15,
    color: '#808080',
  },
  listStyle: {
    height: 300,
    flexGrow: 0,
  },
});

export default SlotSelectionList;
