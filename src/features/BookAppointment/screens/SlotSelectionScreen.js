import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import React, {useState, useEffect} from 'react';
import {Calendar} from 'react-native-calendars';
import {getSlotListByDate} from '../../../redux/actions/slot.actions';
import SlotSelectionList from '../components/SlotSelectionList';
import {
  getMaxDate,
  getMinDate,
  getSelectedDate,
  getTodayDate,
} from '../../Utils/utils';
import { BOOK_SLOT_SCREEN } from '../../../constants/appointment.constant';

export const SlotSelectionScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {slots} = useSelector(state => state.slots);
  const [selected, setSelected] = useState(getTodayDate());
  const [selectedSlot, setSelectedSlot] = useState(null);
  let slotByDateList = slots[getSelectedDate(selected)];

  // Gets he slot by date
  function getSlotByDate(date) {
    dispatch(getSlotListByDate(date));
  }

  useEffect(() => {
    getSlotByDate(getTodayDate());
  }, []);

  // set the selected date, and populate the corresponding slots
  const onPress = date => {
    let selectedDate = getSelectedDate(date.dateString);
    setSelected(date.dateString);
    getSlotByDate(selectedDate);
    setSelectedSlot(null)
  };

  // set the selected slot
  const onSlotSelect = item => {
    setSelectedSlot(item)
  };

  // navigate to book the slot
  const navigateToBookSlot = () => {
    navigation.navigate(BOOK_SLOT_SCREEN, {item: selectedSlot, date: getSelectedDate(selected)});
  };

  return (
    <View style={styles.container}>
      <Calendar
        disableArrowLeft={true}
        hideArrows={true}
        disableMonthChange={true}
        onDayPress={onPress}
        minDate={getMinDate()}
        maxDate={getMaxDate()}
        markedDates={{
          [selected]: {
            selected: true,
            selectedColor: 'orange',
            selectedTextColor: 'red',
          },
        }}></Calendar>
      <Text style={styles.slotText}>Available Slots</Text>
      <SlotSelectionList
        slotByDateList={slotByDateList}
        onSlotSelect={onSlotSelect}
        selectedDate={getSelectedDate(selected)}></SlotSelectionList>
        {selectedSlot ? <TouchableOpacity style={styles.button} onPress={navigateToBookSlot} >
        <Text style={styles.textStyle}>Next</Text>
      </TouchableOpacity> : null}
      
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F1F1F1',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    marginTop: 10,
  },
  slotText: {
    paddingTop: 25,
    fontSize: 16,
  },
  button: {
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: '#808080',
    padding: 10,
    bottom: 40,
    left: 10,
    right: 10,
  },
  textStyle: {
    color: '#ffffff',
  },
});
