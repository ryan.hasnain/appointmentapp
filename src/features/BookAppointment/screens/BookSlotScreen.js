import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text,
} from 'react-native';
import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {bookSlot} from '../../../redux/actions';

export const BookSlotScreen = ({route, navigation}) => {
  const [name, onChangeName] = useState('');
  const [email, onChangeEmail] = useState('');
  const [mobileNumber, onChangeNumber] = useState(null);
  const dispatch = useDispatch();

  // Sends the payload for storing it locally (Async storate), navigate back to home screen
  function book(payload) {
    dispatch(bookSlot(payload));
    navigation.popToTop();
  }


  const onPress = () => {
    if (name != '' && email != '' && mobileNumber != '') {
      let id = route.params.item.id;
      let date = route.params.date;
      const payload = {
        name,
        email,
        mobileNumber,
        id,
        date,
      };
      book(payload);
    }
  };

  return (
    <SafeAreaView>
      <TextInput
        style={styles.input}
        onChangeText={onChangeName}
        placeholder="Name"
        value={name}
      />
      <TextInput
        style={styles.input}
        onChangeText={onChangeEmail}
        value={email}
        placeholder="Email Address"
      />
      <TextInput
        style={styles.input}
        onChangeText={onChangeNumber}
        value={mobileNumber}
        placeholder="Mobile"
        keyboardType="numeric"
      />
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Text style={styles.text}>Book Appoinment</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#808080',
    padding: 10,
    margin: 12,
  },
  text: {
    color: '#ffffff',
  },
});
