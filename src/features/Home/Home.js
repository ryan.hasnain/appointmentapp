import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import { SLOT_SELECTION_SCREEN } from '../../constants/appointment.constant';

export const Home = ({navigation}) => {

  // navigate to the slot selection screen
  const onPress = () => {
    navigation.navigate(SLOT_SELECTION_SCREEN);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Text style={styles.text}>Book Appoinment</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#808080',
    padding: 10,
  },
  text: {
    color: '#ffffff',
  },
});
