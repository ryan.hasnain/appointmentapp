export function getDefaultSlotList() {
  return [
    {id: 'qddfggvb', title: '8AM - 10AM', isAvailable: true},
    {id: 'qeefggvb', title: '10AM - 12PM', isAvailable: true},
    {id: 'qdfggfvb', title: '12PM - 2PM', isAvailable: true},
    {id: 'qdtggfvb', title: '2PM - 4PM', isAvailable: true},
    {id: 'edrfggvb', title: '4PM - 6PM', isAvailable: true},
    {id: 'edrdddvb', title: '4PM - 6PM', isAvailable: true},
  ];
}