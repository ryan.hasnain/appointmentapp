import {getDefaultSlotList} from './slotData';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Methods available for getting and booking slots
export const slotService = {
  getSlotListByDate,
  postBookedSlot,
};

// This method get the slots by date, returns slots (key, value)
async function getSlotListByDate(date) {
  let bookedSlot = await getBookedSlots(date);
  if (bookedSlot) {
    return {
      [date]: getUpdatedSlotList(bookedSlot),
    };
  } else {
    return {
      [date]: getDefaultSlotList(),
    };
  }
}

// This method saves the new slot in async storage
async function postBookedSlot(item) {
  let key = item.date;
  let value = [item];
  let bookedSlot = await getBookedSlots(key);
  if (bookedSlot) {
    if (bookedSlot.some(bookedItem => bookedItem.id != item.id)) {
      bookedSlot.push(item);
    }
    storeSlots(key, bookedSlot);
  } else {
    storeSlots(key, value).then;
  }
}

// This method stores the slot
const storeSlots = async (key, val) => {
  try {
    const jsonValue = JSON.stringify(val);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    console.log(r);
  }
};

// This method gets the booked slot for async storage
const getBookedSlots = async key => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
  }
};

// This method update the default the slot list
function getUpdatedSlotList(bookedSlots) {
  let slotList = getDefaultSlotList();
  let updatedArray = slotList.map(item => {
    return {
      id: item.id,
      title: item.title,
      isAvailable: !bookedSlots.some(bookedItem => bookedItem.id == item.id),
    };
  });
  return updatedArray;
}
