import {GET_SlOT_FAILURE, GET_SlOT_SUCCESS} from '../types/types';

const initialState = {
  hasError: false,
  slots: {},
};

export function slotReducer(state = initialState, action) {
  switch (action.type) {
    case GET_SlOT_SUCCESS: {
      return {
        ...state,
        slots: {...action.payload},
      };
    }
    case GET_SlOT_FAILURE: {
      return {
        ...state,
        hasError: action.payload,
      };
    }
    default:
      return state;
  }
}
