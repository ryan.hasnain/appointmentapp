import { BOOK_SLOT_FAILURE, BOOK_SLOT_SUCCESS } from "../types/types";

const initialState = {
  isSlotBooked: false,
};

export function slotBookedReducer(state = initialState, action) {
  switch (action.type) {
    case BOOK_SLOT_SUCCESS: {
      return {
        ...state,
        isSlotBooked: true
      };
    }
    case BOOK_SLOT_FAILURE: {
      return {
        ...state,
        isSlotBooked: false
      };
    }
    default:
      return state
  }
};