import {combineReducers} from 'redux';
import {slotReducer} from './slot.reducer';
import {slotBookedReducer} from "./bookslot.reducer"

export const rootReducer = combineReducers({
  slots: slotReducer,
  bookedSlot: slotBookedReducer
});