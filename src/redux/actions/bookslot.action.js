import {slotService} from '../../services';
import {BOOK_SLOT_FAILURE, BOOK_SLOT_SUCCESS} from '../types/types';

// saves the selected slot
export function bookSlot(item) {
  return dispatch => {
    return slotService.postBookedSlot(item).then(
      response => {
        dispatch(bookSlotSuccess());
      },
      error => {
        dispatch(bookSlotFailure(error));
      },
    );
  };
}

export const bookSlotSuccess = () => {
  return {
    type: BOOK_SLOT_SUCCESS,
    payload: 'Success',
  };
};

export const bookSlotFailure = () => {
  return {
    type: BOOK_SLOT_FAILURE,
    payload: 'Error',
  };
};
