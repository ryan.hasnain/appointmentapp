import {slotService} from '../../services';
import {GET_SlOT_FAILURE, GET_SlOT_SUCCESS} from '../types/types';

// gets the slot list for the selected date
export function getSlotListByDate(date) {
  return dispatch => {
    return slotService.getSlotListByDate(date).then(
      response => {
        dispatch(getSlotsByDateSuccess(response));
      },
      error => {
        dispatch(getSlotsByDateFalure(error));
      },
    );
  };
}

export const getSlotsByDateSuccess = slots => {
  return {
    type: GET_SlOT_SUCCESS,
    payload: slots,
  };
};

export const getSlotsByDateFalure = error => {
  return {
    type: GET_SlOT_FAILURE,
    payload: error,
  };
};
